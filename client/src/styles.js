import styled from "styled-components";
import { Link } from "react-router-dom";

//1 shared styles
//2 styles for TransactionContainer
//3 styles for TransactionPage

//SharedStyles

export const StyledDiv = styled.div`
  width: 90%;
  background-color: white;
  padding: 2%;
  margin: 5% auto;
  border-radius: 5px;
`;

export const StyledLink = styled(Link)`
  text-decoration: none;
  &:focus,
  &:visited,
  &:link {
    text-decoration: none;
    color: black;
  }
  &:active {
    color: #159189;
  }
`;

export const Container = styled.div`
  margin: 5%;
  @media screen and (min-width: 700px) {
    width: 60%;
    margin: 5% auto;
  }
  @media screen and (min-width: 900px) {
    width: 40%;
    margin: 5% auto;
  }
`;

export const BackButton = styled.button`
  padding: 2%;
  background-color: white;
  color: #159189;
  border: none;
  letter-spacing: 1px;
  border: 2px solid #159189;
  border-radius: 5px;
  font-size: 1rem;
  margin-right: 5%;
  margin-bottom: 2%;
  &:focus,
  &:visited,
  &:link {
    text-decoration: none;
  }
  &:active {
    background-color: #159189;
    color: white;
  }
`;

//TransactionsContainer

export const TransactionsPageContainer = styled.div`
  width: 95%;
  margin: 5% auto;
  @media screen and (min-width: 700px) {
    width: 60%;
  }
  @media screen and (min-width: 900px) {
    width: 40%;
  }
`;

export const Para = styled.p`
  color: black;
  &:link {
    text-decoration: none;
  }
`;

export const SortByRecentRadioBtn = styled.input`
  display: none;
  text-align: center;
`;

export const SortByStatusRadioBtn = styled.input`
  display: none;
  text-align: center;
`;

export const SortButtonOn = styled.label`
  display: table;
  height: 4vh;
  padding: 2%;
  color: black;
  width: 40%;
  background: white;
  border: 2px solid white;
  border-radius: 10px;
  text-align: center;
  line-height: 4vh;
`;

export const SortButtonOff = styled.label`
  display: table;
  height: 4vh;
  padding: 2%;
  width: 40%;
  color: white;
  border: 2px solid white;
  border-radius: 10px;
  text-align: center;
  line-height: 4vh;
`;

export const Form = styled.form`
  display: flex;
  justify-content: space-between;
  margin: auto;
`;

//styles for TransactionPage
