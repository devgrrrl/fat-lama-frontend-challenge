import React from "react";
import axios from "axios";
import styled from "styled-components";

const Button = styled.button`
  padding: 2%;
  background-color: white;
  color: #159189;
  border: 2px solid #159189;
  border-radius: 5px;
  font-size: 1rem;
  margin-right: 5%;
  margin-bottom: 2%;
  &:active {
    background-color: #159189;
    color: white;
  }
  @media screen and (min-width: 700px) {
    width: 60%;
    letter-spacing: 1px;
  }
`
const approveTransaction = (id, cb) => {
  axios
    .put(`http://localhost:8080/transaction/${id}`, {
      status: "FL_APPROVED"
    })
    .then(res => cb(res))
    .catch(err => {
      console.log("there was a problem with the server", err);
    });
};

const ApproveButton = props => {
  const id = props.id;
  const handleApprove = () => {
    approveTransaction(id, function(res) {
      //check props.status !== Cancelled etc, before updating
      console.log("transaction aproved", res.data);
      props.setTransactionStatus(res.data.status);
    });
  };

  return <Button onClick={handleApprove}>APPROVE TRANSACTION</Button>;
};
export default ApproveButton;
