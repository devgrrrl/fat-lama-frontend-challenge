import React from "react";
import TransactionStubLink from "./TransactionStubLink";
import date from "../helpers/date";

const formatDate = d => {
  return date(d);
};

const TransactionStub = props => {
  const results = props.results;
  return (
    <React.Fragment>
      {results &&
        results.map(e => {
          return (
            //returns individual transaction "links"
            <TransactionStubLink
              key={e.id}
              id={e.id}
              status={e.status}
              fromDate={formatDate(e.fromDate)}
            />
          );
        })}
    </React.Fragment>
  );
};
export default TransactionStub;
