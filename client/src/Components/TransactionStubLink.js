import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const P = styled.p`
  width: 90%;
  margin: 2% auto;
  padding: 1%;
  @media screen and (min-width: 700px) {
    margin: 0;
  }
`;

const Stub = styled.div`
  background-color: white;
  margin-top: 3%;
  margin-bottom: 3%;
  padding: 3%;
  border-radius: 10px;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  &:focus,
  &:visited,
  &:link {
    text-decoration: none;
    color:black;
  }
  &:active {
    color: #159189;
  }
`;

const TransactionStubLink = props => {
  return (
    <Stub>
      <StyledLink to={`/transaction/${props.id}`}>
        <P>Transaction Id: {props.id}</P>
        <P>From Date: {props.fromDate}</P>
        <P>Status: {props.status}</P>
      </StyledLink>
    </Stub>
  );
};
export default TransactionStubLink;
