import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
  width: 80%;
  margin: 20% auto;
  text-align: center;
  @media screen and (min-width: 700px) {
    width: 60%;
  }
`;

const StyledLink = styled(Link)`
  font-size: 1.5rem;
  background-color: white;
  padding: 5%;
  border-radius: 5px;
  text-decoration: none;
  &:focus,
  &:visited,
  &:link {
    text-decoration: none;
    color: black;
  }
  &:active {
    color: #159189;
  }
`;
const Home = props => {
  return (
    <Container>
      <StyledLink to={"/transactions/1"}>View Transactions Feed</StyledLink>
    </Container>
  );
};

export default Home;
