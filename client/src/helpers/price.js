const currency = (cost, currency) => {
  const price = cost.toFixed(2);
  return `${price} ${currency}`;
};

export default currency;
