import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import TransactionsContainer from "./Containers/TransactionsContainer";
import TransactionPage from "./Containers/TransactionPage";
import NotFound from "./Components/NotFound";
import Home from "./Components/HomeLink";
import UserPage from "./Containers/UserPage";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route exact path="/" render={props => <Home {...props} />} />
            <Route
              exact
              path="/transactions/:page"
              render={props => <TransactionsContainer {...props} />}
            />
            <Route
              exact
              path="/transaction/:id"
              render={props => <TransactionPage {...props} />}
            />
            <Route
              exact
              path="/user/:id"
              render={props => <UserPage {...props} />}
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
