import axios from "axios";
import React, { Component } from "react";
import TransactionStub from "../Components/TransactionStub";
import "../index.css";
import {
  TransactionsPageContainer,
  Para,
  StyledLink,
  StyledDiv,
  SortByRecentRadioBtn,
  SortByStatusRadioBtn,
  SortButtonOn,
  SortButtonOff,
  Form,
  BackButton
} from "../styles.js";

class TransactionsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      sortOption: "recent",
      feedPage: 1,
      response: ""
    };
    this.handleBack = this.handleBack.bind(this);
  }

  orderByMostRecent() {
    let orderedResults = [...this.state.results];
    return orderedResults.sort((a, b) => {
      return new Date(b.fromDate) - new Date(a.fromDate);
    });
  }

  orderByStatus() {
    let orderedResults = [...this.state.results];
    return orderedResults.sort((a, b) => {
      if (a.status < b.status) return -1;
      else if (a.status > b.status) return 1;
      return 0;
    });
  }

  toggleSort = e => {
    const option = e.target.value;
    this.setState({
      sortOption: option
    });
  };

  pageNotFound = () => {
    this.setState({
      response: "Page not found"
    });
  };

  handleBack() {
    this.props.history.goBack();
  }

  componentDidMount() {
    const page = parseInt(this.props.match.params.page, 10);
    axios
      .get(`http://localhost:8080/transactions/${page}`)
      .then(res => {
        if (res.data.length === 0) {
          return this.pageNotFound();
        } else {
          this.setState({
            results: res.data,
            feedPage: page
          });
        }
      })
      .catch(err => {
        const responseStatus =
          err.response.statusText + " " + err.response.status;
        this.setState({
          response: responseStatus
        });
      });
  }

  render() {
    const results = this.state.results;
    const fetchResults =
      //if there are results sort by time or status
      results.length > 0 ? (
        <div>
          {this.state.sortOption === "recent" ? (
            //order by most recent
            <div>
              <TransactionStub results={this.orderByMostRecent()} />
            </div>
          ) : (
            //order by status
            <div>
              <TransactionStub results={this.orderByStatus()} />
            </div>
          )}
        </div>
      ) : (
        //else check response for error message and display message
        // OR suggest back/page refresh
        //add delay to loader to prevent flashing ?
        <div>
          {this.state.response ? (
            <StyledDiv>
              <p>{this.state.response}</p>
              <BackButton onClick={this.handleBack}>BACK</BackButton>
            </StyledDiv>
          ) : (
            <StyledDiv>
              <p>Loading...</p>
              <p>
                If results don't appear within 30 second, refresh the page or go
                <StyledLink to={"/"}> Home</StyledLink>
              </p>
            </StyledDiv>
          )}
        </div>
      );

    const sortForm = (
      //style label differently depending upon which option is chekced - hide radio button
      <Form>
        {this.state.sortOption === "recent" ? (
          <SortButtonOn htmlFor="sortbymostrecent">Sort By Recent</SortButtonOn>
        ) : (
          <SortButtonOff htmlFor="sortbymostrecent">
            Sort By Recent
          </SortButtonOff>
        )}
        <SortByRecentRadioBtn
          id="sortbymostrecent"
          name="sort"
          checked={this.state.sortOption === "recent"}
          value="recent"
          type="radio"
          onChange={this.toggleSort}
        />
        {this.state.sortOption === "status" ? (
          <SortButtonOn htmlFor="sortbystatus">Sort By Status</SortButtonOn>
        ) : (
          <SortButtonOff htmlFor="sortbystatus">Sort By Status</SortButtonOff>
        )}
        <SortByStatusRadioBtn
          id="sortbystatus"
          name="sort"
          type="radio"
          checked={this.state.sortOption === "status"}
          value="status"
          onChange={this.toggleSort}
        />
      </Form>
    );

    return (
      <TransactionsPageContainer>
        <StyledLink to={"/"}>
          <Para>Home</Para>
        </StyledLink>
        {results.length > 0 && sortForm}
        {fetchResults}
      </TransactionsPageContainer>
    );
  }
}

export default TransactionsContainer;
