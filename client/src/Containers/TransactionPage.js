import React, { Component } from "react";
import axios from "axios";
import ApproveButton from "../Components/ApproveButton";
import date from "../helpers/date";
import price from "../helpers/price";
import { Link } from "react-router-dom";

import { Container, StyledDiv, StyledLink, BackButton } from "../styles.js";

class TransactionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transaction: [],
      transactionStatus: "",
      response: "",
      id: props.match.params.id
    };
    this.handleBack = this.handleBack.bind(this);
  }

  transactionNotFound = () => {
    this.setState({
      response: "Transaction not found"
    });
  };

  setTransactionStatus = res => {
    const status = res;
    this.setState({
      transactionStatus: status
    });
  };

  formatDate(d) {
    return date(d);
  }

  getPrice(cost, currency) {
    return price(cost, currency);
  }

  handleBack() {
    this.props.history.goBack();
  }

  componentDidMount() {
    const result = [];
    axios
      .get(`http://localhost:8080/transaction/${this.state.id}`)
      .then(res => result.push(res.data))
      .then(res => {
        if (result.length === 0) {
          return this.transactionNotFound();
        } else {
          this.setState({
            transaction: result,
            transactionStatus: result[0].status
          });
        }
      })
      .catch(err => {
        const responseStatus =
          err.response.statusText + " " + err.response.status;
        this.setState({
          response: responseStatus
        });
      });
  }
  render() {
    const transaction = this.state.transaction;

    //if results are renders fetchResults contains the transaction details. Else it contains a loading message.
    const fetchResult =
      transaction.length > 0 ? (
        <div>
          <p>Transaction: {transaction[0].id}</p>
          <p>From Date: {this.formatDate(transaction[0].fromDate)}</p>
          <p>Status: {transaction[0].status}</p>
          <p>
            <Link to={`/user/${transaction[0].lenderId}`}>
              {" "}
              Lender Id: {transaction[0].lenderId}
            </Link>
          </p>
          <p>
            <Link to={`/user/${transaction[0].borrowerId}`}>
              {" "}
              Borrower Id: {transaction[0].borrowerId}
            </Link>
          </p>
          <p>Item Id: {transaction[0].itemId}</p>
          <p>To Date: {this.formatDate(transaction[0].toDate)}</p>
          <p>
            Price:{" "}
            {this.getPrice(transaction[0].price, transaction[0].currency)}
          </p>
          <p>Promo Code: {transaction[0].promoCode}</p>
          <p>Total Discount: {transaction[0].totalDiscount}</p>
          <ApproveButton
            id={transaction[0].id}
            setTransactionStatus={this.setTransactionStatus}
          />
          <BackButton onClick={this.handleBack}>BACK</BackButton>
        </div>
      ) : this.state.response ? (
        //if results is empty there is an error message from the axios request to the server, render the errors, otherwise suggest page refres/back
        <div>
          <p>{this.state.response}</p>
          <BackButton onClick={this.handleBack}>BACK</BackButton>
        </div>
      ) : (
        <StyledDiv>
          <p>Loading...</p>
          <p>
            If results don't appear within 30 second, refresh the page or go
            <StyledLink to={"/"}> Home</StyledLink>
          </p>
        </StyledDiv>
      );

    return (
      <Container>
        <StyledLink to={"/"}>Home</StyledLink>
        <StyledDiv>{fetchResult}</StyledDiv>
      </Container>
    );
  }
}
export default TransactionPage;
