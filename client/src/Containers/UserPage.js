import React, { Component } from "react";
import { Container, StyledDiv, StyledLink, BackButton } from "../styles.js";
import axios from "axios";

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: [],
      response: "",
      profileImgUrl: ""
    };
    this.handleBack = this.handleBack.bind(this);
  }

  handleBack() {
    this.props.history.goBack();
  }

  componentDidMount() {
    const result = [];
    axios
      .get(`http://localhost:8080/user/${this.props.match.params.id}`)
      .then(res => result.push(res.data))
      .then(res => {
        if (result.length === 0) {
          console.log("user not found");
        } else {
          this.setState({
            userDetails: result,
            profileImgUrl: result[0].profileImgUrl
          });
        }
      })
      .catch(err => {
        const responseStatus =
          err.response.statusText + " " + err.response.status;
        this.setState({
          response: responseStatus
        });
        console.log("There was an error: ", err);
      });
  }

  render() {
    const userDetails = this.state.userDetails;
    const user =
      this.state.userDetails.length > 0 ? (
        <StyledDiv>
          <img
            src={this.state.profileImgUrl}
            height={100}
            width={100}
            display="block"
            margin="auto"
            vertical-align="top"
            alt=""
          />
          <p>User Id: {this.props.match.params.id}</p>
          <p>First Name: {userDetails[0].firstName}</p>
          <p>Last Name: {userDetails[0].lastName}</p>
          <p>Credit: {userDetails[0].credit}</p>
          <p>Telephone: {userDetails[0].telephone}</p>
          <p>Email: {userDetails[0].email}</p>
          <BackButton onClick={this.handleBack}>Back</BackButton>
        </StyledDiv>
      ) : (
        <StyledDiv>
          <p>{this.state.response}</p>
          <BackButton onClick={this.handleBack}>Back</BackButton>
        </StyledDiv>
      );

    return (
      <Container>
        <StyledLink to={"/"}>Home</StyledLink>
        <StyledDiv>{user}</StyledDiv>
      </Container>
    );
  }
}
export default UserPage;
